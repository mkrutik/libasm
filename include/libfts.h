/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libfts.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zakqin <zakqin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 19:31:13 by zakqin            #+#    #+#             */
/*   Updated: 2019/02/16 23:12:23 by zakqin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTS_H
#define LIBFTS_H

#include <unistd.h>
#include <sys/types.h>

/*
** First part
*/
void	ft_bzero(void *s, size_t n);
char	*ft_strcat(char *dest, const char *src);
int		ft_isalpha(int c);
int		ft_isdigit(int c);
int		ft_isalnum(int c);
int		ft_isascii(int c);
int		ft_isprint(int c);
int		ft_toupper(int c);
int		ft_tolower(int c);
int		ft_puts(const char *s);

/*
** Second part
*/
size_t	ft_strlen(const char *s);
void	*ft_memset(void *s, int c, size_t n);
void	*ft_memcpy(void *dest, const void *src, size_t n);
char	*ft_strdup(const char *s);

/*
** Third part
*/
void	ft_cat(int fd);

/*
** Bonus function
*/
int		ft_isupper(int c);
int		ft_islower(int c);
int		ft_exit(int code);
int		ft_open(const char *pathname, int flags, mode_t mode);
int		ft_close(int fd);
ssize_t	ft_read(int fd, void *buf, size_t count);
ssize_t ft_write(int fd, const void *buf, size_t count);
int		ft_fork();
pid_t	ft_getpid(void);

#endif
