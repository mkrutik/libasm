#include "../include/libfts.h"

#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

#define GREEN	"\033[1;32m"
#define ORANGE 	"\033[0;33m"
#define DEF		"\033[0m"

void	test_ft_isascii(void)
{
	assert(ft_isascii(0));
	assert(ft_isascii(100));
	assert(ft_isascii(127));
	assert(ft_isascii(128) == 0);
	assert(ft_isascii(-1) == 0);
	assert(ft_isascii(-100) == 0);
	assert(ft_isascii(1100) == 0);
	printf("%sft_isascii - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_isdigit(void)
{
	assert(ft_isdigit((int)'0'));
	assert(ft_isdigit((int)'9'));
	assert(ft_isdigit((int)'5'));
	assert(ft_isdigit((int)'A') == 0);
	assert(ft_isdigit((int)'%') == 0);
	assert(ft_isdigit(-100) == 0);
	assert(ft_isdigit(1110) == 0);
	printf("%sft_isdigit - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_isprint(void)
{
	assert(ft_isprint((int)' '));
	assert(ft_isprint((int)'9'));
	assert(ft_isprint((int)'A'));
	assert(ft_isprint((int)'a'));
	assert(ft_isprint((int)'~'));
	assert(ft_isprint(10) == 0);
	assert(ft_isprint(127) == 0);
	printf("%sft_isprint - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_isupper(void)
{
	assert(ft_isupper((int)'A'));
	assert(ft_isupper((int)'B'));
	assert(ft_isupper((int)'Y'));
	assert(ft_isupper((int)'N'));
	assert(ft_isupper((int)'Z'));
	assert(ft_isupper((int)'a') == 0);
	assert(ft_isupper((int)'%') == 0);
	printf("%sft_isupper - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_islower(void)
{
	assert(ft_islower((int)'a'));
	assert(ft_islower((int)'b'));
	assert(ft_islower((int)'y'));
	assert(ft_islower((int)'n'));
	assert(ft_islower((int)'z'));
	assert(ft_islower((int)'A') == 0);
	assert(ft_islower((int)'%') == 0);
	printf("%sft_islower - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_isalpha(void)
{
	assert(ft_isalpha((int)'a'));
	assert(ft_isalpha((int)'z'));
	assert(ft_isalpha((int)'A'));
	assert(ft_isalpha((int)'Z'));
	assert(ft_isalpha((int)'b'));
	assert(ft_isalpha((int)'1') == 0);
	assert(ft_isalpha((int)'%') == 0);
	printf("%sft_isalpha - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_isalnum(void)
{
	assert(ft_isalnum((int)'a'));
	assert(ft_isalnum((int)'z'));
	assert(ft_isalnum((int)'0'));
	assert(ft_isalnum((int)'Z'));
	assert(ft_isalnum((int)'9'));
	assert(ft_isalnum((int)'5'));
	assert(ft_isalnum((int)'%') == 0);
	printf("%sft_isalnum - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_toupper(void)
{
	assert(ft_toupper((int)'a') == (int)'A');
	assert(ft_toupper((int)'z') == (int)'Z');
	assert(ft_toupper((int)'n') == (int)'N');
	assert(ft_toupper((int)'Z') == (int)'Z');
	assert(ft_toupper((int)'5') == (int)'5');
	assert(ft_toupper((int)'%') == (int)'%');
	printf("%sft_toupper - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_tolower(void)
{
	assert(ft_tolower((int)'A') == (int)'a');
	assert(ft_tolower((int)'Z') == (int)'z');
	assert(ft_tolower((int)'N') == (int)'n');
	assert(ft_tolower((int)'z') == (int)'z');
	assert(ft_tolower((int)'5') == (int)'5');
	assert(ft_tolower((int)'%') == (int)'%');
	printf("%sft_tolower - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_strlen(void)
{
	assert(ft_strlen("Hello") == 5);
	assert(ft_strlen("a") == 1);
	assert(ft_strlen("123456789") == 9);
	assert(ft_strlen("") == 0);
	assert(ft_strlen("123456789123456789") == 18);
	printf("%sft_strlen - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_puts(void)
{
	printf("%sft_puts tests: %s\n", ORANGE, DEF);
	ft_puts("first string");
	ft_puts("second string");
	ft_puts("third string");
	printf("%sft_puts - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_bzero(void)
{
	char p[] = "test string";
	printf("%sft_bzero tests: %s\n", ORANGE, DEF);
	{
		printf("call bzero with size == 0 : before call = \"%s\"", p);
		ft_bzero(p, 0);
		printf(" after call = \"%s\"\n", p);
	}
	{
		printf("free array : before call = \"%s\"", p);
		ft_bzero(p, ft_strlen(p));
		printf(" after call = \"%s\"\n", p);
	}
	{
		ft_bzero(NULL, 10);
	}
	printf("%sft_bzero - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_strcat(void)
{
	char first[50] = "Hello ";
	char second[] = "World!";
	printf("%sft_strcat tests: %s\n", ORANGE, DEF);
	{
		printf("before call. first = \"%s\" second = \"%s\"", first, second);
		ft_strcat(first, second);
		printf("\nafter call. first = \"%s\" second = \"%s\"\n", first, second);
	}
	{
		printf("before call. first = \"%s\" second = \"%s\"", first, "");
		ft_strcat(first, "");
		printf("\nafter call. first = \"%s\" second = \"%s\"\n", first, "");
	}
	printf("%sft_strcat - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_memset(void)
{
	char p[] = "test string";
	printf("%sft_memset tests: %s\n", ORANGE, DEF);
	{
		printf("size == 0 : before call = \"%s\"", p);
		ft_memset(p, '$', 0);
		printf(" after call = \"%s\"\n", p);
	}
	{
		printf("size = len: before call = \"%s\"", p);
		ft_memset(p, '$', ft_strlen(p));
		printf(" after call = \"%s\"\n", p);
	}
	printf("%sft_memset - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_memcpy(void)
{
	char first[20] = "first string";
	char second[20] = "Hello World!";

	printf("%sft_memcpy tests: %s\n", ORANGE, DEF);
	{
		printf("before call. src string = \"%s\", dst = \"%s\"\n", first, second);
		ft_memcpy(first, second, ft_strlen(second));
		printf("after call. src string = \"%s\", dst = \"%s\"\n", first, second);
	}
	{
		char	*another = "Another string";
		printf("size = 0. before call. src string = \"%s\", dst = \"%s\"\n", first, another);
		ft_memcpy(first, another, 0);
		printf("after call. src string = \"%s\", dst = \"%s\"\n", first, another);
	}
	printf("%sft_memcpy - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

void	test_ft_strdup(void)
{
	char p[50] = "test string";
	printf("%sft_strdup tests: %s\n", ORANGE, DEF);
	{
		printf("before call. src string = \"%s\"\n", p);
		char *res = ft_strdup(p);
		printf("after call. src string = \"%s\", new str = \"%s\"\n", p, res);
		free(res);
	}
	{
		p[0] = 0;
		printf("copy emty string before call. src string = \"%s\"\n", p);
		char *res = ft_strdup(p);
		printf("after call. src string = \"%s\", dst = \"%s\"\n", p, res);
		free(res);
	}
	printf("%sft_strdup - %sTESTS PASSED%s\n", ORANGE, GREEN, DEF);
}

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;

	test_ft_isupper();
	test_ft_islower();
	test_ft_isascii();
	test_ft_isdigit();
	test_ft_isprint();
	test_ft_isalpha();
	test_ft_isalnum();
	test_ft_tolower();
	test_ft_toupper();
	test_ft_strlen();
	test_ft_puts();
	test_ft_bzero();
	test_ft_strcat();
	test_ft_memset();
	test_ft_memcpy();
	test_ft_strdup();

	printf("Start cat\n");

	ft_cat(0);

	printf("End cat cat\n");

	if (argc == 2)
	{
		printf("print file %s\n", argv[1]);
		int fd = open(ft_strdup(argv[1]), 0, 0777);
		printf("Fd = %d\n", fd);
		ft_cat(fd);
		ft_close(fd);
	}

	if (ft_fork() != 0) {
		printf("Parrent proc id = %d\n", ft_getpid());
	} else {
		printf("Child proc id = %d\n", ft_getpid());
	}

	ft_exit(-1);
	printf("MSG after exit\n");
	return (0);
}