# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/02/16 19:31:22 by zakqin            #+#    #+#              #
#    Updated: 2019/04/20 16:46:17 by mkrutik          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

RED 	= \033[0;31m
GREEN 	= \033[1;32m
ORANGE 	= \033[0;33m
GRAY 	= \033[1;30m
DEF 	= \033[0m

CC			= gcc
CC_FLAGS    = -Wall -Wextra -Werror

BIN			= bin
INCLUDE		= -I include/

SRC_LIB		=	src/ft_bzero.s \
				src/ft_isascii.s \
				src/ft_isdigit.s \
				src/ft_isprint.s \
				src/ft_isupper.s \
				src/ft_islower.s \
				src/ft_isalpha.s \
				src/ft_isalnum.s \
				src/ft_toupper.s \
				src/ft_tolower.s \
				src/ft_strlen.s \
				src/ft_puts.s \
				src/ft_strcat.s \
				src/ft_exit.s \
				src/ft_fork.s \
				src/ft_read.s \
				src/ft_write.s \
				src/ft_open.s \
				src/ft_close.s \
				src/ft_getpid.s \
				src/ft_memset.s \
				src/ft_memcpy.s \
				src/ft_strdup.s \
				src/ft_cat.s

OBJ_LIB			= $(SRC_LIB:.s=.o)
EXECUTABLE_LIB	= $(BIN)/libfts.a

SRC_TEST 		 = test/main.c
OBJ_TEST 		 = $(SRC_TEST:.c=.o)
EXECUTABLE_TEST = $(BIN)/tests


all:  create_bin $(EXECUTABLE_LIB) $(EXECUTABLE_TEST)
	@ echo "${GREEN}Compile binary file was successful${DEF}"

create_bin:
	@ mkdir -p bin

$(EXECUTABLE_LIB): $(OBJ_LIB)
	@ ar rc $(EXECUTABLE_LIB) $(OBJ_LIB)
	@ ranlib $(EXECUTABLE_LIB)

$(EXECUTABLE_TEST): $(OBJ_TEST) $(EXECUTABLE_LIB)
	@ $(CC)  $(CC_FLAGS) $(OBJ_TEST) -o $(EXECUTABLE_TEST) $(EXECUTABLE_LIB)

# linking: $(EXECUTABLE_LIB)
# 	ld -macosx_version_min 10.13 -lSystem -o libftASM_Test $(OBJ_TEST) $(EXECUTABLE_LIB)


%.o : %.c
	@ $(CC) $(CC_FLAGS) $(INCLUDE) -o $@ -c $<
	@ echo "${GRAY}Compile object files${DEF}"

%.o : %.s
	@ nasm -f macho64 $< # macho64  | elf64
	@ echo "${GRAY}Compile object files${DEF}"

clean:
	@ rm -f $(OBJ_LIB) $(OBJ_TEST)
	@ echo "${ORANGE}Object files was successfuly remowed${DEF}"

fclean: clean
	@ rm -f $(EXECUTABLE_LIB) $(EXECUTABLE_TEST)
	@ rm -rf $(BIN)
	@ echo "${RED}Binary file was successfuly remowed${DEF}"

re: fclean all
