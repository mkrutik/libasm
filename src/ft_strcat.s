extern	_ft_strlen

section	.text
	global	_ft_strcat

_ft_strcat:	; char	*ft_strcat(char *dest, const char *src);
	push	rdi
	call	_ft_strlen

	pop 	rdi
	push	rdi

	add		rdi, rax

my_loop:
	cmp		BYTE[rsi], 0
	je 		return

	mov		cl, BYTE[rsi]
	mov		BYTE[rdi], cl
	add 	rsi, 1
	add 	rdi, 1
	jmp 	my_loop


return:
	mov		BYTE[rdi], 0
	pop		rax
	ret