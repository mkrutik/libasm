extern	_ft_islower

section	.text
	global	_ft_toupper

_ft_toupper:	; int	ft_toupper(int c);
	call	_ft_islower
	cmp		rax, 1
	je		yes

	mov		rax, rdi
	ret

yes:
	sub		rdi, 32
	mov		rax, rdi
	ret