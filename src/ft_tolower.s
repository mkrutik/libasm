extern	_ft_isupper

section	.text
	global	_ft_tolower

_ft_tolower:	; int	ft_tolower(int c);
	call	_ft_isupper
	cmp		rax, 1
	je		yes

	mov		rax, rdi
	ret

yes:
	add		rdi, 32
	mov		rax, rdi
	ret