section	.text
	global	_ft_memset

_ft_memset:	; void	*ft_memset(void *s, int c, size_t n);

	push	rdi

my_loop:
	cmp		rdx, 0
	jle		.return

	mov		BYTE[rdi], sil
	add		rdi, 1
	dec		rdx
	jmp		my_loop

.return:
	pop rax
	ret
