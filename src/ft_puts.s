extern	_ft_strlen
extern	_ft_write

STDOUT equ 1

section	.text
	global	_ft_puts

_ft_puts:	; int	ft_puts(const char *s);
	mov		rsi, rdi
	call	_ft_strlen

	mov		rdx, rax
	mov		rdi, STDOUT
	call	_ft_write
	push	rax

	sub 	rsp, 8
	mov		rdi, STDOUT
	mov		rsi, new_line
	mov		rdx, 1
	call	_ft_write

	add		rsp, 8
	pop 	rax
	inc		rax
	ret

section	.data
	new_line DB 10 ; '\n'
