extern	_ft_memset

section	.text
	global	_ft_bzero

_ft_bzero:	; void	ft_bzero(void *s, size_t n);
	cmp		rdi, 0
	je		return

	mov		rdx, rsi
	mov		rsi, 0
	call	_ft_memset

return:
	ret