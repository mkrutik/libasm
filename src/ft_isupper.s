section	.text
   global	_ft_isupper

MIN equ 65
MAX equ 90

_ft_isupper:	; int	ft_isupper(int c);
	cmp		rdi, MAX
	jg		no
	cmp		rdi, MIN
	jl		no

yes:
	mov		rax, 1
	ret
no:
	mov		rax, 0
	ret
