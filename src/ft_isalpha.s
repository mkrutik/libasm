extern	_ft_isupper
extern	_ft_islower

section	.text
	global	_ft_isalpha

_ft_isalpha:	; int	ft_isalpha(int c);
	call	_ft_isupper
	cmp		rax, 1
	je		return
	call	_ft_islower

return:
	ret