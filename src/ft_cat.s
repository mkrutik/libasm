extern	_ft_read
extern	_ft_write

section	.text
	global	_ft_cat

_ft_cat:
	cmp		rdi, 0
	jl		failed

my_loop:
	push	rdi

	mov		rsi, buff
	mov		rdx, 4095
	call	_ft_read

	cmp		rax, 0
	jle		return

	mov		rdi, STDOUT
	mov		rsi, buff
	mov		rdx, rax
	call	_ft_write

	pop		rdi
	jmp		my_loop


failed:
	ret
return:
	pop		rdi
	ret

STDOUT	equ 1

section	.data
	buff times 4096 db 0

