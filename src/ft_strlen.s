section	.text
	global	_ft_strlen

_ft_strlen:	; size_t	ft_strlen(const char *s);
	mov		rax, 0

my_loop:
	cmp		BYTE[rdi], 0
	je		return

	add		rdi, 1
	inc		rax
	jmp		my_loop

return:
	ret