section	.text
   global _ft_isascii

MIN equ 0
MAX equ 127

_ft_isascii:	; int	ft_isascii(int c);
	cmp		rdi, MAX
	jg		no
	cmp		rdi, MIN
	jl		no

yes:
	mov		rax, 1
	ret
no:
	mov		rax, 0
	ret
