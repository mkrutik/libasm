section .text
	global _ft_read

_ft_read:	; ssize_t	ft_read(int fd, void *buf, size_t count);
	mov		rbx, rdi	; fd
	mov		rcx, rsi	; buff

	mov		eax, 0x2000003
	syscall
	ret