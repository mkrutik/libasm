WRITE_SYS_CALL equ 4

section	.text
   global _ft_write

_ft_write:	; ft_write(int fd, const void *buf, size_t count);
	mov		rbx, rdi	; fd
	mov		rcx, rsi	; buf
	; mov	rdx, rdx	;count

	mov		eax, 0x2000004
	syscall
	ret