section	.text
	global	_ft_isdigit

MIN equ 48
MAX equ 57

_ft_isdigit:	; int	ft_isdigit(int c);
	cmp		rdi, MAX
	jg		no
	cmp		rdi, MIN
	jl		no

yes:
	mov		rax, 1
	ret
no:
	mov		rax, 0
	ret
