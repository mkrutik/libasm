section	.text
	global	_ft_islower

MIN equ 97
MAX equ 122

_ft_islower:	; int ft_isupper(int c);
	cmp		rdi, MAX
	jg		no
	cmp		rdi, MIN
	jl		no

yes:
	mov		rax, 1
	ret
no:
	mov		rax, 0
	ret
