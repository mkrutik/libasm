extern	_ft_isalpha
extern	_ft_isdigit

section	.text
	global	_ft_isalnum

_ft_isalnum:	; int	ft_isalnum(int c);
	call	_ft_isalpha
	cmp		rax, 1
	je		return
	call	_ft_isdigit

return:
	ret
