section	.text
   global _ft_memcpy

MIN equ 0
MAX equ 127

_ft_memcpy:	; void	*ft_memcpy(void *dest, const void *src, size_t n);
	push	rdi

copy_loop:
	cmp		rdx, 0
	jle		return

	dec		rdx
	mov		cl, BYTE[rsi]
	mov		BYTE[rdi], cl
	add		rdi, 1
	add		rsi, 1
	jmp		copy_loop

return:
	pop		rax
	ret