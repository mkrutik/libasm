section	.text
   global	_ft_isprint

MIN equ 32
MAX equ 126

_ft_isprint:	; int	ft_isprint(int c);
	cmp		rdi, MAX
	jg		no
	cmp		rdi, MIN
	jl		no

yes:
	mov		rax, 1
	ret
no:
	mov		rax, 0
	ret
