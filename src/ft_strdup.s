extern	_malloc
extern	_ft_memcpy
extern	_ft_strlen

section	.text
	global	_ft_strdup

_ft_strdup:	;	char	*ft_strdup(const char *s)

	push	rbp
	mov 	rbp, rsp
	push	rdi
	call	_ft_strlen

	inc		rax
	push	rax

	mov		rdi, rax
	call	_malloc

	cmp		rax, 0
	je		failed

	mov		rdi, rax
	pop		rdx
	pop		rsi

	call	_ft_memcpy
	leave
	ret

failed:
	pop		rdx
	pop		rsi
	mov	rax, 0
	leave
	ret